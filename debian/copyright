Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: s3d
Upstream-Contact: s3d-devel@lists.sourceforge.net
Source: http://s3d.sourceforge.net/

Files: *
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
License: GPL-2+

Files: */CMakeLists.txt
  cmake/modules/*.cmake
  CMakeLists.txt
  config.h.cmake
Copyright: 2007-2015, Sven Eckelmann <sven@narfation.org>
License: BSD-3-clause

Files: apps/kism3d/*
Copyright: 2006-2015, Marek Lindner <marek.lindner@mailbox.org>
License: GPL-2+

Files: apps/meshs3d/*
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
           2004-2015, Marek Lindner <marek.lindner@mailbox.org>
           2004-2015, Andreas Langer <an.langer@gmx.de>
License: GPL-2+

Files: apps/s3dosm/*
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
           2004-2015, Marek Lindner <marek.lindner@mailbox.org>
           2004-2015, Andreas Langer <an.langer@gmx.de>
           2008-2015, Sven Eckelmann <sven@narfation.org>
License: GPL-2+

Files: apps/s3dosm/http_*
Copyright: 2001, Lyle Hanson <lhanson@cs.nmu.edu>
License: GPL-2+

Files: apps/s3dvt/*
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
           2002, Alexander Graf <helly@gmx.net>
License: GPL-2+

Files: libs3d/*
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
           2004-2015, Marek Lindner <marek.lindner@mailbox.org>
           2007-2015, Sven Eckelmann <sven@narfation.org>
License: LGPL-2.1+

Files: libs3d/s3d_keysym.h
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
           1997-2004, Sam Lantinga <slouken@libsdl.org>
License: LGPL-2.1+

Files: libs3d/sei_*
Copyright: 2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
           1994, Atul Narkhede <narkhede@cs.unc.edu>
           1994, Dinesh Manocha <dm@cs.unc.edu>
License: LGPL-2.1+

Files: libs3dw/*
Copyright: 2006-2008, Simon Wunderlich <sw@simonwunderlich.de>
License: LGPL-2.1+

Files: server/*
Copyright: 2007-2015, Sven Eckelmann <sven@narfation.org>
           2006-2015, Marek Lindner <marek.lindner@mailbox.org>
           2004-2015, Simon Wunderlich <sw@simonwunderlich.de>
License: GPL-2+

Files: objs/*
Copyright: 2006-2015, Andreas Langer <an.langer@gmx.de>
           2006-2015, Christiane Weidauer <yd@freakkind.de>
           2006-2015, Mike Graenz <mog@users.berlios.de>
           2006-2015, Simon Wunderlich <sw@simonwunderlich.de>
License: GPL-2+

Files: libs3dw/libs3dw.pc.cmake
  libs3dw/libs3dw.ver
  libs3d/libs3d.ver
  ConfigureChecks.cmake
Copyright: 2007-2015, Sven Eckelmann <sven@narfation.org>
License: BSD-3-clause

Files: extras/mplayer/mplayer.1.0pre7try2.s3d.patch
  extras/swig/Makefile
Copyright: 2007-2015, Simon Wunderlich <sw@simonwunderlich.de>
License: BSD-3-clause

Files: Documentation/manpages/build-manpages.sh
  Documentation/xml.doxygen
  libs3d/libs3d.pc.cmake
  s3drc.cmake
Copyright: 2007-2015, Sven Eckelmann <sven@narfation.org>
License: CC0-1.0

Files: debian/*
Copyright: 2008-2022, Simon Wunderlich <sw@simonwunderlich.de>
           2008-2022, Sven Eckelmann <sven@narfation.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems the full text of the GNU Lesser General Public License can be
 found in the `/usr/share/common-licenses/LGPL-2.1' file.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The names of Kitware, Inc., the Insight Consortium, or the names of
    any consortium members, or of any contributors, may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in ‘/usr/share/common-licenses/CC0-1.0’.
